/**
 * @author Sahan Dinuka
 * @CreatedBy IntelliJ IDEA
 * @created 14/05/2022 - 1:48 PM
 */
const express = require('express');
const product = require('./api/product');
const app = express();
const PORT = process.env.PORT || 5050;
app.use("/api/product", product);
app.listen(PORT, () => console.log(`server is running on port ${PORT}`));
