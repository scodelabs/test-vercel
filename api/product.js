/**
 * @author Sahan Dinuka
 * @CreatedBy IntelliJ IDEA
 * @created 14/05/2022 - 2:00 PM
 */
const express = require('express');
const router = express.Router();

router.get("/", async (req, res) => {
    try {
        res.json({
            status: 200,
            message: "Get data has successfully"
        });
    } catch (e) {
        console.log(e);
        return res.status(500).send("server error");
    }
});

module.exports = router;
